const axios = require('axios');
const cheerio = require('cheerio');
const Promise = require('bluebird');
const fs = require('fs');
const url = 'https://www.cermati.com';

let urls = [],
    json = {},
    datas = []
axios.get(url + '/artikel')
  .then((response) => {
    const $ = cheerio.load(response.data);
    $('div.list-of-articles > div.article-list-item').each((i, value) => {
      urls[i] = $(value).find('a').attr('href');
    });
    Promise.each(urls, (value, index, arrayLength) => {
      return axios.get(url + value)
        .then((response) => {
          const $ = cheerio.load(response.data);
          $('section.post-content').each((item, element) => {
            datas.push({
              url: url + value,
              title: $(element).find('h1').text(),
              author: $(element).find('span.author-name').text().trim(),
              postingDate: $(element).find('span.post-date').text().trim(),
              relatedArticles: [],
            })
          })
        })
        .catch((err) => {
          console.log(err)
        });
    }).then((result) => {
      Promise.each(datas, (value, index, arrayLength) => {
        return axios.get(value.url)
          .then((response) => {
            const $ = cheerio.load(response.data);
            $('div.side-list-panel').each((i, el) => {
              let artikelTerkait = $(el).find('h4').first().text();
              artikelTerkait = artikelTerkait.toLowerCase().trim();
              artikelTerkait = artikelTerkait.replace(/\s/g, '');
              $(el).find('ul.panel-items-list li').each((item, element) => {
                if (artikelTerkait == 'artikelterkait') {
                  datas[index]['relatedArticles'][item] = {
                    url: $(element).find('a').attr('href'),
                    title: $(element).find('h5').text(),
                  };
                }
              })
            })
          })
          .catch((err) => {
            console.log(err)
          });
      }).then((result) => {
        json.articles = datas;
        fs.writeFile('./solution.json',
          JSON.stringify(json, null, 2), (err) => {
            console.log('Write scrapping success!')
          })
      }).catch((err) => {
        console.log(err)
      })
    }).catch((err) => {
      console.log(err)
    })
  })
  .catch((err) => {
    console.log(err)
  });