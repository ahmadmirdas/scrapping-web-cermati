const axios = require('axios');
const cheerio = require('cheerio');
const Promise = require('bluebird');
const fs = require('fs');
const url = 'https://www.cermati.com';

let urls = [],
    json = {},
    datas = []
axios.get(url + '/artikel')
  .then((response) => {
    const $ = cheerio.load(response.data);
    $('div.list-of-articles > div.article-list-item').each((i, value) => {
      urls[i] = $(value).find('a').attr('href');
    });
    Promise.each(urls, (value, index, arrayLength) => {
      return axios.get(url + value)
        .then((response) => {
          const $ = cheerio.load(response.data);
          $('section.post-content').each((item, element) => {
            datas.push({
              url: url + value,
              title: $(element).find('h1').text(),
              author: $(element).find('span.author-name').text().trim(),
              postingDate: $(element).find('span.post-date').text().trim(),
              relatedArticles: [],
            })
          })
        })
        .catch((err) => {
          console.log(err)
        });
    }).then((result) => {
      let getDetailPage = function (detailspage) {
        return new Promise(function (resolve, reject) {
          axios.get(detailspage.url)
            .then((response) => {
              const $ = cheerio.load(response.data);
              $('div.side-list-panel').each((i, el) => {
                let artikelTerkait = $(el).find('h4').first().text();
                artikelTerkait = artikelTerkait.toLowerCase().trim();
                artikelTerkait = artikelTerkait.replace(/\s/g, '');
                $(el).find('ul.panel-items-list li').each((item, element) => {
                  if (artikelTerkait == 'artikelterkait') {
                    detailspage.relatedArticles.push({
                    url: $(element).find('a').attr('href'),
                    title: $(element).find('h5').text(),
                    });
                  }
                })
              })
              resolve();
            })
            .catch((err) => {
              reject (err)
            });
        });
      };
      let detailPages = datas.map(getDetailPage);
      Promise.all(detailPages)
        .then(() => {
          json.articles = datas;
          fs.writeFile('./solution.json',
              JSON.stringify(json, null, 2), (err) => {
              console.log('Write scrapping success!')
              })
        })
        .catch(function (err) {
          console.log(err);
        });
    }).catch((err) => {
      console.log(err)
    })
  })
  .catch((err) => {
    console.log(err)
  });